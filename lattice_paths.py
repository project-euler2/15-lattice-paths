import math as m
def find_number_paths(grid_size) :
    return 2*(m.factorial(2*(grid_size - 1))/(m.factorial(grid_size - 2)*m.factorial(grid_size))) + 2 * (m.factorial(2*(grid_size - 1))/(m.factorial(grid_size - 1)*m.factorial(grid_size - 1)))

print(find_number_paths(20))